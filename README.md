# ArtiAdm

ArtiAdm is a CLI API to manage an Artifactory SaaS instance.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install artiadm.

```bash
python3 -m pip install --index-url=https://alon.jfrog.io/artifactory/api/pypi/pypi/simple artiadm
```

## Usage

```python
python3 -m artiadm [-h] command

API CLI in Python to manage an Artifactory SaaS instance

optional arguments:
  -h, --help   show this help message and exit

These are commands to manage an Artifactory SaaS instance:
  command
    ping       Get a simple status response about the state of Artifactory
    version    Retrieve information about the current Artifactory version, revision, and
               currently installed Add-ons
    createuser
               Creates user in Artifactory
    deleteuser
               Removes an Artifactory user
    storageinfo
               Returns storage summary information regarding binaries, file store and
               repositories
```