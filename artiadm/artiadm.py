#!/usr/bin/env python

import argparse
import requests
import json

ARTIFACTORY_API_BASE_URL = "https://alon.jfrog.io/artifactory/api"
# The Token is placed here, though it is not good to have Secrets hard-coded. A better solution would be using
# K8S Secrets, and using it as an environment variable
ARTIFACTORY_ACCESS_TOKEN = "AKCp8hyZKiCVLtGKQ8chgp9dojHokZv4kYWV7yLB4t8dVzSRmsqNVXJP8rYGx2hd3yRQwQ9cM"
API_REQUEST_HEADERS = {"X-JFrog-Art-Api": ARTIFACTORY_ACCESS_TOKEN,
                       "Content-Type": "application/json"}


def cli():
    """
        This function is the main function of the cli.
        It creates the parser, the subcommands parser and help description of all commands, and calls the right
        function according to the command provided.
    """
    parser = argparse.ArgumentParser(
        description='API CLI in Python to manage an Artifactory SaaS instance',
        prog='artiadm')
    subparsers = parser.add_subparsers(title='These are commands to manage an Artifactory SaaS instance',
                                       metavar='command')
    # create the parser for the "ping" command
    ping_parser = subparsers.add_parser('ping', help='Get a simple status response about the state of Artifactory',
                                        description='Get a simple status response about the state of Artifactory')
    ping_parser.set_defaults(command=ping)
    # create the parser for the "version" command
    version_parser = subparsers.add_parser('version', help='Retrieve information about the current Artifactory'
                                                           ' version, revision, and currently installed Add-ons',
                                           description='Retrieve information about the current Artifactory'
                                                           ' version, revision, and currently installed Add-ons')
    version_parser.set_defaults(command=version)
    # create the parser for the "createuser" command
    createuser_parser = subparsers.add_parser('createuser', help='Creates user in Artifactory',
                                              description='Creates user in Artifactory')
    createuser_parser.add_argument("username", type=str, help='The name of the user to be created')
    createuser_parser.add_argument("email", type=str, help='The email of the user to be created')
    createuser_parser.add_argument("password", type=str, help='The password of the user to be created')
    createuser_parser.set_defaults(command=createuser)
    # create the parser for the "deleteuser" command
    deleteuser_parser = subparsers.add_parser('deleteuser', help='Removes an Artifactory user',
                                              description='Removes an Artifactory user')
    deleteuser_parser.add_argument("username", type=str, help='The name of the user to be deleted')
    deleteuser_parser.set_defaults(command=deleteuser)
    # create the parser for the "storageinfo" command
    storageinfo_parser = subparsers.add_parser('storageinfo', help='Returns storage summary information regarding'
                                                                   ' binaries, file store and repositories',
                                               description='Returns storage summary information regarding'
                                                                   ' binaries, file store and repositories')
    storageinfo_parser.set_defaults(command=storageinfo)
    # parse subcommand and its attributes
    args = parser.parse_args()
    if hasattr(args, 'command'):
        args.command(args)
    else:
        parser.print_help()


def ping(args):
    """
        This function handles the ping command. It sends an API request to the Artifactory instance and
        prints the system ping status.
    """
    try:
        request = requests.get(ARTIFACTORY_API_BASE_URL + "/system/ping",
                               headers=API_REQUEST_HEADERS)
        print(request.text)

    except ConnectionError as err:
        print("Failed to fetch system ping: %", err, ', Error code : ', request.status_code)


def version(args):
    """
        This function handles the version command. It sends an API request to the Artifactory instance and
        prints the system version.
    """
    try:
        request = requests.get(ARTIFACTORY_API_BASE_URL + "/system/version",
                               headers=API_REQUEST_HEADERS)
        print(json.dumps(request.json(), indent=1))

    except ConnectionError as err:
        print("Failed to fetch system version: %", err, ', Error code : ', request.status_code)


def createuser(args):
    """
        This function handles the createuser command. It sends an API request to the Artifactory instance and
        creates a user with the data provided through the CLI.
    """
    username = args.username
    user_data = {
        "email": args.email,
        "password": args.password
    }
    try:
        request = requests.put(ARTIFACTORY_API_BASE_URL + "/security/users/" + username,
                               headers=API_REQUEST_HEADERS, data=json.dumps(user_data))
        if request.status_code == 201:
            print("Created a user named " + username)
        else:
            print(json.dumps(request.json(), indent=1))

    except ConnectionError as err:
        print("Failed to create user: %", err, ', Error code : ', request.status_code)


def deleteuser(args):
    """
        This function handles the deleteuser command. It sends an API request to the Artifactory instance and
        removes a user with the username provided through the CLI.
    """
    username = args.username
    try:
        request = requests.delete(ARTIFACTORY_API_BASE_URL + "/security/users/" + username,
                                  headers=API_REQUEST_HEADERS)
        if request.status_code == 200:
            print(request.text)
        else:
            print(json.dumps(request.json(), indent=1))

    except ConnectionError as err:
        print("Failed to delete user: %", err, ', Error code : ', request.status_code)


def storageinfo(args):
    """
        This function handles the storageinfo command. It sends an API request to the Artifactory instance and
        prints the system's storage info.
    """
    try:
        request = requests.get(ARTIFACTORY_API_BASE_URL + "/storageinfo",
                               headers=API_REQUEST_HEADERS)
        print(json.dumps(request.json(), indent=1))

    except ConnectionError as err:
        print("Failed to fetch storage info: %", err, ', Error code : ', request.status_code)


if __name__ == '__main__':
    cli()
