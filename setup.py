import setuptools

setuptools.setup(
    name="artiadm", # Replace with your own username
    version="0.0.1",
    author="Alon Kahana",
    author_email="ak2012biu@gmail.com",
    description="A CLI API for managing Artifactory SaaS instance",
    url="https://gitlab.com/alka4747/artiadm",
    license='',
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    install_requires=[
     'requests'
    ]
)